import React, { useEffect } from 'react';
import { AuthProvider } from 'providers/auth';
import { ProfileProvider } from 'providers/profile';

import AuthModule, { routes as AuthRoutes } from 'modules/auth';
import UsersModule, { routes as UsersRoutes } from 'modules/users';

import SalaryModule from 'modules/salary';

import { GlobalStyle } from './styled';

import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import { api } from 'services/api';
import { BrowserRouter } from 'react-router-dom';
import Root from 'core/root';
import { renderRoutes } from 'react-router-config';

const theme = createMuiTheme({
  typography: {
    h4: {
      fontWeight: '200',
    },
    h5: {
      fontWeight: '200',
    },
  },
  palette: {
    primary: {
      main: '#4b76a8',
    },
  },
});

console.info(
  `🌎 Welcome to RNA project. Now you in ${process.env.NODE_ENV} mode.`
);

const routes = [
  {
    component: Root,
    routes: [
      {
        path: '/',
        exact: true,
        component: SalaryModule,
      },
      { path: '/auth', component: AuthModule, routes: AuthRoutes },
      { path: '/users', component: UsersModule, routes: UsersRoutes },
    ],
  },
];

function Application() {
  useEffect(() => {
    api.v1
      .get('/')
      .then((response) => {
        console.log('Init API finished', response.data);
      })
      .catch((error) => {
        console.error('Init API failed', error);
      });
  }, []);
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <AuthProvider>
          <ProfileProvider>
            <GlobalStyle />
            {renderRoutes(routes)}
          </ProfileProvider>
        </AuthProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default Application;
