import React from 'react';
import { useAuth } from 'providers/auth';
import { Redirect } from 'react-router-dom';

import WelcomeTemplate from 'uikit/templates/welcome';
import Registration from './registration';
import Login from './login';
import { renderRoutes } from 'react-router-config';

function AuthModule({ route }) {
  const auth = useAuth();
  if (auth.state.token) {
    return <Redirect to={'/'} />;
  }

  return <WelcomeTemplate>{renderRoutes(route.routes)}</WelcomeTemplate>;
}

export const URLS = {
  LOGIN: '/auth/login',
  REGISTRATION: '/auth/registration',
};

export const routes = [
  {
    path: URLS.LOGIN,
    component: Login,
  },
  {
    path: URLS.REGISTRATION,
    component: Registration,
  },
];

export default AuthModule;
