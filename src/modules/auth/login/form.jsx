import React, { useCallback, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { TextField as MUITextField, InputAdornment } from '@material-ui/core';
import { api } from 'services/api';
import Button from 'uikit/button';

import styled from 'styled-components';
import { Person, Lock } from '@material-ui/icons';
import { useAuth } from 'providers/auth';

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
`;

const TextField = styled(MUITextField)`
  && {
    margin-bottom: 20px;
  }
`;

function LoginForm() {
  const { control, handleSubmit } = useForm({ mode: 'onChange' });
  const [loading, setLoading] = useState(false);
  const auth = useAuth();

  const onSubmit = useCallback((data) => {
    setLoading(true);
    api.v1
      .post('/auth/login', data)
      .then((response) => {
        const { token } = response.data;
        auth.methods.setToken(token);
      })
      .catch((err) => {
        alert(err);
      })
      .finally(() => {
        setLoading(false);
      });
  });

  return (
    <StyledForm onSubmit={handleSubmit(onSubmit)}>
      <Controller
        as={TextField}
        control={control}
        name="login"
        variant="outlined"
        label={'Имя пользователя'}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <Person fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
      />
      <Controller
        as={TextField}
        control={control}
        name="password"
        variant="outlined"
        label={'Пароль'}
        type="password"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <Lock fontSize="small" color="primary" />
            </InputAdornment>
          ),
        }}
      />
      <Button
        loading={loading}
        variant="contained"
        color="primary"
        size="large"
        type="submit"
      >
        Войти
      </Button>
    </StyledForm>
  );
}

export default LoginForm;
