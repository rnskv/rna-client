import React, { useEffect, useCallback } from 'react';
import { useAuth } from 'providers/auth';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Typography,
} from '@material-ui/core';
import LoginForm from './form';
import { LockOpen } from '@material-ui/icons';
import { URLS } from '../index';
import { useHistory } from 'react-router-dom';
import VKButton from '../components/vk-button';
import { Box } from './styled';

function Registration() {
  const auth = useAuth();
  const history = useHistory();

  const goToRegistration = useCallback(() => {
    history.push(URLS.REGISTRATION);
  });

  return (
    <div style={{ maxWidth: '90%', width: '400px' }}>
      <Card raised style={{ width: '100%' }}>
        <CardHeader
          titleTypographyProps={{ variant: 'h4' }}
          title="Авторизация"
          avatar={<LockOpen />}
        />
        <CardContent>
          <LoginForm />
        </CardContent>
        <CardActions>
          <Button size="small" color="primary" onClick={goToRegistration}>
            Регистрация
          </Button>
        </CardActions>
      </Card>
      <Box>
        <Typography
          color="textSecondary"
          variant="body2"
          style={{ margin: '20px 0 10px 0' }}
        >
          <b>ИЛИ</b>
        </Typography>
        <VKButton />
      </Box>
    </div>
  );
}

export default Registration;
