import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  text-align: center;
`;
