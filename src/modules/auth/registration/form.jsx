import React, { useCallback, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { TextField as MUITextField } from '@material-ui/core';
import Button from 'uikit/button';

import styled from 'styled-components';
import { yupResolver } from '@hookform/resolvers';
import { schema } from './validation';
import get from 'lodash/get';
import { api } from 'services/api';
import { URLS } from '../index';
import { useHistory } from 'react-router-dom';

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
`;

const TextField = styled(MUITextField)`
  && {
    margin-bottom: 20px;
  }
`;

function RegistrationForm() {
  const history = useHistory();
  const { control, handleSubmit, errors, formState } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(schema),
  });

  const [loading, setLoading] = useState(false);

  const onSubmit = useCallback((data) => {
    setLoading(true);
    api.v1
      .post('/registration/local', data)
      .then((response) => {
        console.log(response);
        history.push(URLS.LOGIN);
      })
      .catch((err) => {
        alert(err);
      })
      .finally(() => {
        setLoading(false);
      });
  });

  return (
    <div>
      <StyledForm onSubmit={handleSubmit(onSubmit)}>
        <Controller
          as={TextField}
          control={control}
          name="login"
          variant="outlined"
          label={'Имя пользователя'}
          error={get(errors, 'login')}
          helperText={get(errors, 'login')?.message}
          required
        />
        <Controller
          as={TextField}
          control={control}
          name="password"
          variant="outlined"
          label={'Пароль'}
          type="password"
          error={get(errors, 'password')}
          helperText={get(errors, 'password')?.message}
          required
        />
        <Controller
          as={TextField}
          control={control}
          name="email"
          variant="outlined"
          label={'Электронная почта'}
          error={get(errors, 'email')}
          helperText={get(errors, 'email')?.message}
          required
        />
        <Controller
          as={TextField}
          control={control}
          name="name"
          variant="outlined"
          label={'Имя'}
          error={get(errors, 'name')}
          helperText={get(errors, 'name')?.message}
          required
        />
        <Controller
          as={TextField}
          control={control}
          name="lastname"
          variant="outlined"
          label={'Фамилия'}
          error={get(errors, 'lastname')}
          helperText={get(errors, 'lastname')?.message}
          required
        />
        <Controller
          as={TextField}
          control={control}
          name="age"
          variant="outlined"
          inputProps={{ type: 'number' }}
          label={'Возраст'}
          error={get(errors, 'age')}
          helperText={get(errors, 'age')?.message}
          required
        />
        <Controller
          as={TextField}
          control={control}
          name="salary"
          variant="outlined"
          type="number"
          label={'Ежемесячный доход'}
          error={get(errors, 'salary')}
          helperText={get(errors, 'salary')?.message}
          required
        />

        <Button
          loading={loading}
          variant="contained"
          color="primary"
          size="large"
          type="submit"
        >
          Зарегистрироваться
        </Button>
      </StyledForm>
    </div>
  );
}

export default RegistrationForm;
