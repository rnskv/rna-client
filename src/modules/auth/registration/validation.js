import * as yup from 'yup';

export const schema = yup.object().shape({
  login: yup.string().required(),
  name: yup.string().required(),
  email: yup.string().email().required(),
  lastname: yup.string().required(),
  age: yup
    .number()
    .positive()
    .integer()
    .required()
    .min(14)
    .max(100)
    .nullable()
    .transform((v, o) => (o === '' ? null : v)),
  salary: yup
    .number()
    .positive()
    .required()
    .nullable()
    .transform((v, o) => (o === '' ? null : v)),
});
