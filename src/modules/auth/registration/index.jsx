import React, { useCallback, useEffect } from 'react';
import { useAuth } from 'providers/auth';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
} from '@material-ui/core';
import LoginForm from './form';
import { LockOpen } from '@material-ui/icons';
import { URLS } from '../index';
import { useHistory } from 'react-router-dom';

function Registration() {
  const auth = useAuth();
  const history = useHistory();

  const goToLogin = useCallback(() => {
    history.push(URLS.LOGIN);
  });

  return (
    <Card raised style={{ width: '400px', maxWidth: '90%' }}>
      <CardHeader
        titleTypographyProps={{ variant: 'h4' }}
        title="Регистрация"
        avatar={<LockOpen />}
      />
      <CardContent>
        <LoginForm />
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={goToLogin}>
          Авторизация
        </Button>
      </CardActions>
    </Card>
  );
}

export default Registration;
