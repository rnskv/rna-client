import React, { useCallback, useEffect, useState } from 'react';
import { useLocation } from 'react-router';
import qs from 'query-string';
import { api } from 'services/api';
import { useAuth } from 'providers/auth';
import Button from 'uikit/button';
import VkIcon from '@iconify/icons-mdi/vk';
import { Icon } from '@iconify/react';

const VKButton = ({ style, className }) => {
  const { REACT_APP_VK_APP_ID, REACT_APP_HOST } = process.env;
  const cbLink = `${REACT_APP_HOST}/auth/login`;
  const auth = useAuth();
  const [loading, setLoading] = useState(null);

  const { search } = useLocation();
  const { code, state } = qs.parse(search);

  console.log(REACT_APP_VK_APP_ID);

  const handleRedirect = useCallback(() => {
    window.location.href = `https://oauth.vk.com/authorize?client_id=${REACT_APP_VK_APP_ID}&display=popup&redirect_uri=${cbLink}&scope=email&response_type=code&v=5.120&state=4194308`;
  });

  const handleLogin = useCallback(
    (code) => {
      api.v1
        .post('/auth/login/vk', { code })
        .then((response) => {
          const { token } = response.data;
          auth.methods.setToken(token);
        })
        .catch((err) => {
          alert(err);
          //   window.location.href = cbLink;
        })
        .finally(() => {
          setLoading(false);
        });
    },
    [auth.methods]
  );

  useEffect(() => {
    if (code) {
      handleLogin(code);
    }

    console.log('PARAMS', code, state);
  }, [code]);

  return (
    <Button
      style={style}
      className={className}
      loading={loading}
      onClick={handleRedirect}
      startIcon={<Icon color="#4b76a8" width="40" height="40" icon={VkIcon} />}
    >
      ВКонтакте
    </Button>
  );
};

export default VKButton;
