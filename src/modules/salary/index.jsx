import React, { useCallback, useEffect, useState } from 'react';
import { Paper, Typography, Divider, Grid, Avatar } from '@material-ui/core';
import { Content, MiniBox, Controls, Title, AvatarContainer } from './styled';
import Button from 'uikit/button';

import Drawer from '@material-ui/core/Drawer';
import SalaryChanger from './salary-changer';
import RelatedUsers from './related-users';
import SalaryList from 'uikit/salary-list';
import Advertisement from 'uikit/advertisement';
import IncomeCounter from 'uikit/income-counter';
import Skeleton from '@material-ui/lab/Skeleton';

import { format } from 'helpers/money';
import { useProfile } from 'providers/profile';

import HomeTemplate from 'uikit/templates/home';
import { useAuth } from 'providers/auth';
import { Redirect } from 'react-router-dom';

const Salary = () => {
  const profile = useProfile();
  const auth = useAuth();

  const [openedDrawer, setOpenedDrawer] = useState(false);

  const openDrawer = useCallback(() => {
    setOpenedDrawer(true);
  });

  const closeDrawer = useCallback(() => {
    setOpenedDrawer(false);
  });

  if (!auth.state.token) {
    return <Redirect to={'/auth/login'} />;
  }

  if (profile.state.error) {
    return (
      <HomeTemplate>
        <div
          style={{ marginTop: '92px', maxWidth: '360px', textAlign: 'center' }}
        >
          <Typography variant="h5">
            Произошла ошибка, попробуйте зайти позже
          </Typography>
        </div>
      </HomeTemplate>
    );
  }

  return (
    <HomeTemplate>
      <div
        style={{
          marginTop: '92px',
          maxWidth: '360px',
          width: '100%',
          overflow: 'hidden',
        }}
      >
        <Advertisement />
        <Paper elevation={3} style={{ marginTop: '40px' }}>
          <Content>
            <Grid style={{ padding: '20px' }} container alignItems="center">
              <Grid item xs>
                <Typography variant="h5">Доход:</Typography>
              </Grid>
              <Grid item>
                <Typography variant="h4" color="primary">
                  {!profile.state.loading ? (
                    format(profile.state.data.salary)
                  ) : (
                    <Skeleton width={190} />
                  )}
                </Typography>
              </Grid>
            </Grid>
            <Divider />
            <AvatarContainer>
              {!profile.state.loading ? (
                <Avatar
                  style={{ width: '100px', height: '100px' }}
                  src={profile.state.data.photo}
                >
                  {`${profile.state.data.name[0]}${profile.state.data.lastname[0]}`}
                </Avatar>
              ) : (
                <Skeleton variant="circle" width={100} height={100} />
              )}
            </AvatarContainer>
            <IncomeCounter
              salary={profile.state.data.salary}
              loading={profile.state.loading}
            />
            <Divider />
            <SalaryList
              loading={profile.state.loading}
              salary={profile.state.data.salary}
            />
            <Divider />
            <Controls>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={openDrawer}
              >
                Указать другой доход
              </Button>
            </Controls>
          </Content>
        </Paper>
        <Title variant="h4">Самые богатые:</Title>
        <RelatedUsers redirectTo="/users/rich" sortBy="salary" sortRule={-1} />
        <Title variant="h4">Новые:</Title>
        <RelatedUsers redirectTo="/users/new" sortBy="_id" sortRule={-1} />
      </div>
      <Drawer anchor={'bottom'} open={openedDrawer} onClose={closeDrawer}>
        <MiniBox textAlign="center">
          <Typography variant="h4">Сменить доход</Typography>
        </MiniBox>
        <SalaryChanger />
      </Drawer>
    </HomeTemplate>
  );
};

export default Salary;
