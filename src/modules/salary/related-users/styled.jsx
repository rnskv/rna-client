import styled from 'styled-components';

export const Avatars = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 10px;
`;
