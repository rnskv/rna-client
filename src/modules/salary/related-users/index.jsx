import React, { useCallback, useState, useMemo } from 'react';
import { Paper, Avatar, Dialog, Typography } from '@material-ui/core';
import Button from 'uikit/button';

import { Avatars } from './styled';
import Profile from 'uikit/profile';
import BackButton from 'uikit/back-button';
import useUsers from 'hooks/useUsers';
import Skeleton from '@material-ui/lab/Skeleton';
import { useProfile } from 'providers/profile';
import Slide from '@material-ui/core/Slide';
import { useHistory, useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';

const RelatedUsers = ({
  redirectTo,
  children,
  loading,
  disabled,
  sortBy,
  sortRule,
  ...props
}) => {
  const location = useLocation();
  const profileId = useMemo(
    () => new URLSearchParams(location.search).get(`profile`),
    [location.search]
  );
  const profile = useProfile();
  const history = useHistory();

  console.log(profileId);
  const showProfile = useCallback(
    (userId) => () => {
      console.log('set open profile', location);
      history.push({
        pathname: location.pathname,
        search: `?profile=${userId}`,
      });
    },
    []
  );

  const hideProfile = useCallback(() => {
    console.log('set close profile');
    history.push(location.pathname);
  }, []);

  const users = useUsers({
    exclude: [profile.state.data._id],
    page: 1,
    numsOnPage: 5,
    sortBy,
    sortRule,
    ignore: profile.state.loading,
  });

  return (
    <div>
      <Paper elevation="3">
        {users.loading ? (
          <Avatars>
            <Skeleton variant="circle" width={40} height={40} />
            <Skeleton variant="circle" width={40} height={40} />
            <Skeleton variant="circle" width={40} height={40} />
            <Skeleton variant="circle" width={40} height={40} />
          </Avatars>
        ) : (
          <Avatars>
            {users.list.map((user, index) => (
              <Button key={index} onClick={showProfile(user._id)}>
                <Avatar src={user.photo}>{user.name[0]}</Avatar>
              </Button>
            ))}
          </Avatars>
        )}
        <Button
          style={{ width: '100%', borderRadius: 0 }}
          variant="contained"
          color="primary"
          component={Link}
          to={redirectTo}
        >
          {!users.loading ? 'Показать ещё' : <Skeleton width={200} />}
        </Button>
      </Paper>
      {profileId && (
        <Dialog
          TransitionComponent={Slide}
          minWidth="xl"
          fullScreen
          onClose={hideProfile}
          open={!!profileId}
        >
          <BackButton onClick={hideProfile} />
          <Profile userId={profileId} />
        </Dialog>
      )}
    </div>
  );
};

export default RelatedUsers;
