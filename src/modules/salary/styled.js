import styled from 'styled-components';
import { Typography } from '@material-ui/core';

export const Content = styled.div``;

export const MiniBox = styled.div`
  padding: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Controls = styled.div`
  display: flex;
  justify-content: center;
  padding: 20px;
`;

export const Title = styled(Typography)`
  padding: 20px 0;
`;

export const AvatarContainer = styled.div`
  margin: 20px auto 0;
  width: 100px;
`;
