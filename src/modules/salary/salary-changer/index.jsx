import React, { useCallback, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { TextField as MUITextField, InputAdornment } from '@material-ui/core';
import Button from 'uikit/button';

import styled from 'styled-components';
import { Person, Lock } from '@material-ui/icons';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import { api } from 'services/api';
import { MiniBox } from '../styled';
import { useProfile } from 'providers/profile';

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  padding: 40px;
  align-items: center;
`;

const Box = styled.div`
  max-width: 360px;
  width: 100vw;
  display: flex;
  flex-direction: column;
`;

const TextField = styled(MUITextField)``;

function SalaryChanger() {
  const { control, handleSubmit } = useForm({ mode: 'onChange' });
  const [loading, setLoading] = useState(false);
  const profile = useProfile();

  const onSubmit = useCallback((data) => {
    setLoading(true);

    api.v1
      .put('/users/salary', { salary: data.salary })
      .then((response) => {
        console.log('/profile/salary response:', response);
        profile.methods.load();
      })
      .catch((err) => {
        alert(err);
      })
      .finally(() => {
        setLoading(false);
      });
  });

  return (
    <StyledForm onSubmit={handleSubmit(onSubmit)}>
      <Box>
        <Controller
          as={TextField}
          control={control}
          name="salary"
          variant="outlined"
          label={'Доход'}
          type="number"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AccountBalanceWalletIcon fontSize="small" color="primary" />
              </InputAdornment>
            ),
          }}
        />
        <Button
          loading={loading}
          style={{ marginTop: '20px' }}
          variant="contained"
          color="primary"
          size="large"
          type="submit"
        >
          Сохранить
        </Button>
      </Box>
    </StyledForm>
  );
}

export default SalaryChanger;
