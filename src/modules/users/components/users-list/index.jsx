import React, { useState, useCallback } from 'react';
import useUsers from 'hooks/useUsers';
import { useProfile } from 'providers/profile';
import User from '../../components/user';
import { List } from './styled';
import Button from 'uikit/button';
import CircularProgress from '@material-ui/core/CircularProgress';
import BackButton from 'uikit/back-button';
import { useHistory } from 'react-router-dom';

const UsersList = ({ sortRule, sortBy }) => {
  const showOnPage = 4;
  const [count, setCount] = useState(showOnPage);
  const profile = useProfile();
  const history = useHistory();
  const users = useUsers({
    exclude: [profile.state.data._id],
    page: 1,
    numsOnPage: count,
    sortBy,
    sortRule,
    ignore: profile.state.loading,
  });

  const showMore = useCallback(() => {
    setCount(count + showOnPage);
  });

  const goBack = useCallback(() => {
    history.push('/');
  });

  return (
    <div
      style={{
        position: 'relative',
        marginTop: '52px',
        alignItems: 'center',
        flexDirection: 'column',
        display: 'flex',
      }}
    >
      <div style={{ width: '100%' }}>
        <BackButton isStatic onClick={goBack} />
      </div>

      <List>
        {!users.list.length && users.loading ? (
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              padding: '100px 0',
            }}
          >
            <CircularProgress size={50} />
          </div>
        ) : (
          users.list.map((user) => <User key={user.id} user={user} />)
        )}
      </List>
      <Button
        size="large"
        variant="contained"
        color="primary"
        onClick={showMore}
        loading={users.loading}
        style={{ marginTop: '15px' }}
      >
        Показать еще
      </Button>
    </div>
  );
};

export default UsersList;
