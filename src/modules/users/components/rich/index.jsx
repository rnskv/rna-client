import React from 'react';
import UsersList from '../users-list';

const RichUsers = () => {
  return <UsersList sortBy="salary" sortRule={-1} />;
};

export default RichUsers;
