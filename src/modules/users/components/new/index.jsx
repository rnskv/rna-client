import React from 'react';
import UsersList from '../users-list';

const NewUsers = () => {
  return <UsersList sortBy="_id" sortRule={-1} />;
};

export default NewUsers;
