import React from 'react';
import useUsers from 'hooks/useUsers';
import { useProfile } from 'providers/profile';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IncomeCounter from 'uikit/income-counter';
import Button from 'uikit/button';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { format } from 'helpers/money';
import { Typography } from '@material-ui/core';

const User = ({ user }) => {
  return (
    <Card
      style={{
        marginBottom: '10px',
        width: '90%',
      }}
    >
      <div
        style={{
          textAlign: 'center',
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
          padding: '15px 0 0 0',
        }}
      >
        <Typography
          gutterBottom
          variant="h4"
        >{`${user.name} ${user.lastname[0]}.`}</Typography>
        <Avatar
          style={{
            width: '100px',
            height: '100px',
            margin: '15px',
          }}
          src={user.photo}
        />
        <Typography variant="body2">Доход:</Typography>
        <Typography variant="h5">{format(user.salary)}</Typography>
      </div>
      <CardMedia>
        <IncomeCounter
          withCoins={false}
          title="Сегодня заработал"
          //   size="small"
          loading={false}
          salary={user.salary}
        />
      </CardMedia>
      <CardActions
        style={{ display: 'flex', padding: '20px 0', justifyContent: 'center' }}
        disableSpacing
      >
        <Button variant="outlined" size="large" color="primary">
          Посмотреть профиль
        </Button>
      </CardActions>
    </Card>
  );
};

export default User;
