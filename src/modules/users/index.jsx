import React from 'react';
import { useAuth } from 'providers/auth';

import HomeTemplate from 'uikit/templates/home';
import { renderRoutes } from 'react-router-config';
import RichUsers from './components/rich';
import NewUsers from './components/new';

function UsersModule({ route }) {
  return <HomeTemplate>{renderRoutes(route.routes)}</HomeTemplate>;
}

export const URLS = {
  RICH: '/users/rich',
  NEW: '/users/new',
};

export const routes = [
  {
    path: URLS.RICH,
    // eslint-disable-next-line react/display-name
    component: RichUsers,
  },
  {
    path: URLS.NEW,
    // eslint-disable-next-line react/display-name
    component: NewUsers,
  },
];

export default UsersModule;
