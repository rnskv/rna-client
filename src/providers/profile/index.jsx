import React, {
  useContext,
  useState,
  createContext,
  useEffect,
  useCallback,
} from 'react';
import { useAuth } from 'providers/auth';
import { api } from 'services/api';

const defaultProfileState = {
  data: {},
  loading: true,
  error: null,
};

export const ProfileContext = createContext(defaultProfileState);

export const ProfileProvider = ({ children }) => {
  const auth = useAuth();
  const [loading, setLoading] = useState(defaultProfileState.loading);
  const [error, setError] = useState(defaultProfileState.error);
  const [data, setData] = useState(defaultProfileState.data);

  const handleLoad = useCallback(() => {
    api.v1
      .get('/users/profile')
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    if (!auth.state.token) return;
    handleLoad();
  }, [auth.state.token, handleLoad]);

  const state = {
    data,
    error,
    loading,
  };

  const methods = {
    load: () => {
      handleLoad();
    },
  };

  return (
    <ProfileContext.Provider value={{ state, methods }}>
      {children}
    </ProfileContext.Provider>
  );
};

export const useProfile = () => {
  return useContext(ProfileContext);
};
