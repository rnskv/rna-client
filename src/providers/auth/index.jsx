import React, {
  useState,
  createContext,
  useContext,
  useCallback,
  useEffect,
} from 'react';
import Cookies from 'js-cookie';

const defaultAuthState = {
  token: Cookies.get('token'),
};

export const AuthContext = createContext(defaultAuthState);

export const AuthProvider = ({ children, onChangeToken }) => {
  const [authState, setAuthState] = useState(defaultAuthState);

  useEffect(() => {
    console.info('Auth provider handle token changing');
    onChangeToken && onChangeToken(authState);
  }, [authState, authState.token, onChangeToken]);

  const setToken = useCallback(
    (token) => {
      Cookies.set('token', token);
      setAuthState({
        ...authState,
        token,
      });
    },
    [authState]
  );

  const logout = useCallback(() => {
    Cookies.remove('token');

    setAuthState({
      ...authState,
      token: null,
    });
  });

  return (
    <AuthContext.Provider
      value={{ state: authState, methods: { setToken, logout } }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
