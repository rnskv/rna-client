const { createProxyMiddleware } = require('http-proxy-middleware');
const morgan = require('morgan');

module.exports = (app) => {
  console.log('set proxy', process.env.REACT_APP_API_URL);

  app.use(
    '/api',
    createProxyMiddleware({
      target: process.env.REACT_APP_API_URL,
      changeOrigin: true,
      pathRewrite: {
        '^/api': '/',
      },
    })
  );

  app.use(morgan('combined'));
};
