import { useEffect, useState } from 'react';
import { api } from 'services/api';
import qs from 'query-string';

const useUsers = ({ exclude, numsOnPage, page, sortBy, sortRule, ignore }) => {
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const params = qs.stringify({
    exclude,
    page,
    numsOnPage,
    sortRule,
    sortBy,
  });

  useEffect(() => {
    if (ignore) return;

    setLoading(true);
    api.v1
      .get(`/users/list?${params}`)
      .then((response) => {
        console.log('/users/salary response:', response);
        setList(response.data);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [ignore, params]);

  return {
    error,
    loading,
    list,
  };
};

export default useUsers;
