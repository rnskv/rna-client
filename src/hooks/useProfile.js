import { useEffect, useState } from 'react';

const useProfile = () => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);

    setTimeout(() => {
      setData({
        salary: 10000,
        photo:
          'https://biz.liga.net/images/general/2019/07/17/thumbnail-tw-20190717155005-8015.jpg?v=1563373586',
        bio: `Родился 28 октября 1955 года в Сиэтле. Свою первую программу для игры в крестики нолики Гейтс сочинил в 13 лет. В 15 лет он вместе с Алленом написал программу для регулирования уличного движения, и получил заработок в 20 000 долларов.`,
        name: 'Билл',
        lastname: 'Гейтс',
      });

      setLoading(false);
      setError(null);
    }, 1504);
  }, []);

  return {
    error,
    loading,
    data,
  };
};

export default useProfile;
