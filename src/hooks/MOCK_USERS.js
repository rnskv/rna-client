const MOCK = [
  {
    id: 1,
    salary: 30000,
    photo:
      'https://sun9-71.userapi.com/impg/gZ7dfjb5uLxbajuSZqr7epIs87Zxr_jOJZEKqA/5ITis5s04CU.jpg?size=200x0&quality=88&crop=331,571,937,937&sign=0932c22eaa05925edba7d227d14a9973&ava=1',
    bio: `Просто Андрей`,
    name: 'Андрей',
    lastname: 'Лисов',
  },
  {
    id: 2,
    salary: 45000,
    photo:
      'https://sun9-56.userapi.com/impf/c841433/v841433631/50d4e/85-odThiOwo.jpg?size=200x0&quality=88&crop=0,159,960,960&sign=6cffd2fea42989b9d59db766d6b47093&ava=1',
    bio: `Сережа, просто Серж`,
    name: 'Сергей',
    lastname: 'Бородин',
  },
  {
    id: 3,
    salary: 45600000000,
    photo:
      'https://biz.liga.net/images/general/2019/07/17/thumbnail-tw-20190717155005-8015.jpg?v=1563373586',
    bio: `Уи́льям Ге́нри Гейтс III (англ. William Henry Gates III; 28 октября 1955, Сиэтл[6], Вашингтон), более известный как Билл Гейтс (англ. Bill Gates)[7][8] — американский предприниматель и общественный деятель, филантроп, один из создателей (совместно с Полом Алленом) и бывший крупнейший акционер компании Microsoft. До июня 2008 года являлся руководителем компании, после ухода с поста остался в должности её неисполнительного председателя совета директоров. Также является сопредседателем благотворительного Фонда Билла и Мелинды Гейтс, членом совета директоров Berkshire Hathaway, ген. директор Cascade investment.`,
    name: 'Билл',
    lastname: 'Гейтс',
    wiki:
      'https://ru.wikipedia.org/wiki/%D0%93%D0%B5%D0%B9%D1%82%D1%81,_%D0%91%D0%B8%D0%BB%D0%BB',
  },
  {
    id: 4,
    salary: 2000,
    photo:
      'https://sun9-52.userapi.com/impg/6Z58mk9VsYiobS8wE-gImDZP6ipb5FWK4qEzrw/c0kiPg0LT_c.jpg?size=200x0&quality=88&crop=0,45,805,805&sign=4610ee36a43f36f32d0d04f29c8d6a4c&ava=1',
    bio: `Мишутка, рыжий, агрессор, часто выступает в качестве подставного лица, берет удар на себя. Время от времени отхыватывает, но в основном выходит из воды сухим`,
    name: 'Миша',
    lastname: 'Петров',
  },
  {
    id: 5,
    salary: 100,
    photo: null,
    bio: `Инкогнито.`,
    name: 'Кто то',
    lastname: 'Тот самый',
  },
];

export default MOCK;
