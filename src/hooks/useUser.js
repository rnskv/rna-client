import { useEffect, useState } from 'react';
import { api } from 'services/api';

const useUser = ({ id }) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    api.v1
      .get(`/users/${id}`)
      .then((response) => {
        console.log('/users/:id response:', response);
        setData(response.data);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [id]);

  return {
    error,
    loading,
    data,
  };
};

export default useUser;
