import Cookies from 'js-cookie';

export const processRequestSuccess = (config) => {
  config.headers['Authorization'] = 'Bearer ' + Cookies.get('token');
  return config;
};

export const processRequestFail = (error) => error;

export const processResponseSuccess = (response) => response;

export const processResponseFail = (error) => {
  if (error.response) {
    const { status } = error.response;

    if (status === 401) {
      // token error
    }
  }

  return Promise.reject(error);
};
