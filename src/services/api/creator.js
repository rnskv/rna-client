import axios from 'axios';
import qs from 'qs';

import {
  processRequestSuccess,
  processRequestFail,
  processResponseSuccess,
  processResponseFail,
} from './middlewares';

const createApi = ({ url, timeout = 60 * 1000 }) => {
  const api = axios.create({
    baseURL: url,
    paramsSerializer: qs.stringify,
    timeout: timeout,
  });

  api.interceptors.request.use(processRequestSuccess, processRequestFail);

  api.interceptors.response.use(processResponseSuccess, processResponseFail);

  return api;
};

export default createApi;
