import createApi from './creator';

export const api = {
  v1: createApi({
    url: '/api',
  }),
};
