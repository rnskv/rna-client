export const format = (value, options = { fixed: 2 }) => {
  const number = Number(value).toFixed(options.fixed);
  const trunc = Math.trunc(number);
  const float = (number - trunc)
    .toFixed(options.fixed)
    .toString()
    .split('.')[1];

  const formatted = trunc
    .toString()
    .split(/(?=(?:\d{3})+(?!\d))/)
    .join(' ');

  return `${formatted}.${float}₽`;
};

export const getSalaryVariants = (salary) => {
  const monthly = salary;
  const weekly = salary / 4;
  const daily = weekly / 7;
  const hoursly = daily / 24;
  const minutely = hoursly / 60;
  const secondly = minutely / 60;
  const yearly = monthly * 12;
  const millisecondly = secondly / 1000;

  return {
    secondly,
    minutely,
    daily,
    weekly,
    monthly,
    yearly,
    hoursly,
    millisecondly,
  };
};
