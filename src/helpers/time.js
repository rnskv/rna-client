export const getMillisecondsToday = () => {
  const now = new Date();
  const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

  const diff = now - today;
  return diff;
};
