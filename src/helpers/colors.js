function djb2(str) {
  let hash = 9999;
  for (let i = 0; i < str.length; i++) {
    hash = (hash << 5) + hash + str.charCodeAt(i); /* hash * 33 + c */
  }
  return hash;
}

export const stringToHex = (str) => {
  const hash = djb2(str);
  const r = (hash & 0xff0000) >> 16;
  const g = (hash & 0x00ff00) >> 8;
  const b = hash & 0x0000ff;
  const hex = `#${`0${r.toString(16)}`.substr(-2)}${`0${g.toString(16)}`.substr(
    -2
  )}${`0${b.toString(16)}`.substr(-2)}`;

  return hex;
};
