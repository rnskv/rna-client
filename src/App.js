import React from 'react';
import Application from './core/application';

function App() {
  return (
    <div className="App">
      <Application />
    </div>
  );
}

export default App;
