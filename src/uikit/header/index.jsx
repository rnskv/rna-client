import React from 'react';
import { Box } from './styled';
import { AppBar, Typography } from '@material-ui/core';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';

const Header = () => {
  return (
    <AppBar position="fixed">
      <Box style={{ padding: '10px' }}>
        <MonetizationOnIcon size="big" />
        <Typography variant="h5" style={{ marginLeft: '5px' }}>
          Money<b>Gram</b>
        </Typography>
      </Box>
    </AppBar>
  );
};

export default Header;
