import styled from 'styled-components';

export const Box = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;
`;
