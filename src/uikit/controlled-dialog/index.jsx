import React, { useEffect, useMemo } from 'react';
import { Dialog as MUIDialog } from '@material-ui/core';
import { useHistory, useLocation } from 'react-router';

const ControlledDialog = ({ name, children, open, onClose, ...props }) => {
  const history = useHistory();
  const location = useLocation();

  const pathname = useMemo(() => location.pathname, []);
  const search = useMemo(() => location.search, []);

  const opened = useMemo(
    () => new URLSearchParams(location.search).get(`${name}_open`),
    [location.search]
  );

  useEffect(() => {
    console.log('useEfect open', name, open);
    if (open) {
      history.push({
        pathname: pathname,
        search: `${search ? `${search}&` : '?'}${name}_open=${open}`,
      });
    } else {
      const queryParams = new URLSearchParams(search);
      queryParams.delete(`${name}_open`);
      console.log('use close');
      history.push({
        pathname: pathname,
        search: queryParams.toString(),
      });
    }
  }, [open]);

  useEffect(() => {
    if (!opened) {
      onClose();
    }
  }, [opened]);

  return (
    <MUIDialog onClose={onClose} open={opened} {...props}>
      {children}
    </MUIDialog>
  );
};

export default ControlledDialog;
