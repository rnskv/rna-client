import PropTypes from 'prop-types';
import React from 'react';
import TodayIcon from '@material-ui/icons/Today';
import DateRangeIcon from '@material-ui/icons/DateRange';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import Alert from '@material-ui/lab/Alert';
import { format } from 'helpers/money';
import SalaryListItem from './item';

import { getSalaryVariants } from 'helpers/money';

const SalaryList = ({ salary, loading }) => {
  const minimal = 11163;
  const difference = salary - minimal;

  const { daily, weekly, monthly, minutely } = getSalaryVariants(salary);

  const list = [
    {
      value: minutely,
      in: 'минуту',
      Icon: QueryBuilderIcon,
    },
    {
      value: daily,
      in: 'день',
      Icon: TodayIcon,
    },
    {
      value: weekly,
      in: 'неделю',
      Icon: CalendarTodayIcon,
    },
    {
      value: monthly,
      in: 'месяц',
      Icon: DateRangeIcon,
    },
  ];

  return (
    <div>
      {!loading && (
        <div>
          {difference > 0 ? (
            difference < 5000 ? (
              <Alert severity="info">
                Доход больше прожиточного минимума на {format(difference)}.
                Возможно стоит задуматься о смене работодателя
              </Alert>
            ) : (
              <Alert severity="success">
                Доход больше прожиточного минимума на {format(difference)}
              </Alert>
            )
          ) : difference === 0 ? (
            <Alert severity="warning">
              Доход является прожиточным минимумом!
            </Alert>
          ) : (
            <Alert severity="error">
              Доход меньше прожиточного минимума. Cоветуем обратиться с жалобой
              к работодателю
            </Alert>
          )}
        </div>
      )}

      {/* <PossiblePurchases salary={salary} /> */}

      {!loading &&
        list.map((item) => (
          <SalaryListItem key={item.in} item={item} loading={loading} />
        ))}
    </div>
  );
};

SalaryList.propTypes = {
  salary: PropTypes.number,
};

SalaryList.defaultProps = {
  salary: 0,
};

export default SalaryList;
