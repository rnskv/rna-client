import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { MenuItem, Dialog } from '@material-ui/core';
import { MiniBox, Box } from './styled';
import { format } from 'helpers/money';
import PossiblePurchases from 'uikit/possible-purchases';
import BackButton from 'uikit/back-button';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Skeleton from '@material-ui/lab/Skeleton';

const SalaryListItem = ({ item, loading }) => {
  const [open, setOpen] = useState(false);

  const onOpen = useCallback(() => {
    setOpen(true);
  });

  const onClose = useCallback(() => {
    setOpen(false);
  });

  return (
    <>
      <MenuItem onClick={onOpen}>
        <MiniBox>
          <item.Icon color="primary" />
        </MiniBox>
        <MiniBox>
          {loading ? (
            <Skeleton width={200} />
          ) : (
            `${format(item.value)} / ${item.in}`
          )}
        </MiniBox>
        <MiniBox style={{ margin: '0 0 0 auto' }}>
          <ArrowForwardIosIcon color="primary" />
        </MiniBox>
      </MenuItem>
      <Dialog fullWidth open={open} onClose={onClose}>
        <BackButton onClick={onClose} />
        <Box>
          <PossiblePurchases salary={item.value} />
        </Box>
      </Dialog>
    </>
  );
};

export default SalaryListItem;
