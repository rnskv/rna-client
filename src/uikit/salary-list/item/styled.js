import styled from 'styled-components';

export const MiniBox = styled.div`
  padding: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Box = styled.div`
  max-width: 100%;
  margin: 0 auto;
  box-sizing: border-box;
  width: 360px;
`;
