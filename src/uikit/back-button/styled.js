import styled from 'styled-components';

export const GoBack = styled.div`
  display: flex;
  align-items: center;
  padding: ${({ isStatic }) => (isStatic ? '15px 0' : '40px 0')};
  h5 {
    margin-left: 10px;
  }

  && {
    .MuiButton-label {
      color: #4b76a8;
    }
  }

  &:hover {
    color: red;
  }
`;
