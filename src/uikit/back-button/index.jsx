import React from 'react';
import { Typography, Button } from '@material-ui/core';
import { GoBack } from './styled';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

const BackButton = ({ onClick, isStatic }) => {
  return (
    <GoBack isStatic={isStatic} onClick={onClick}>
      <Button
        disableRipple
        style={{
          position: !isStatic ? 'fixed' : 'inherit',
          background: !isStatic ? '#ffffffc2' : 'transparent',
          zIndex: 9,
        }}
      >
        <ArrowBackIcon /> <Typography variant="h5">Вернуться</Typography>
      </Button>
    </GoBack>
  );
};

export default BackButton;
