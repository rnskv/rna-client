import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Card, CardMedia, Button } from '@material-ui/core';
import { Alert } from './styled';

const Advertisement = ({ userId }) => {
  return (
    <Card elevation={3} style={{ maxWidth: '360px', margin: '0 auto' }}>
      <CardMedia>
        <iframe
          width="100%"
          height="auto"
          src="https://www.youtube.com/embed/dQw4w9WgXcQ"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
          autoPlay
        ></iframe>
      </CardMedia>
      <Alert size="small" severity="info">
        Размещено на правах рекламы
      </Alert>
    </Card>
  );
};

export default Advertisement;
