import styled from 'styled-components';
import MUIAlert from '@material-ui/lab/Alert';

export const Alert = styled(MUIAlert)`
  && {
    display: flex;
    align-items: center;

    .MuiAlert-icon {
      font-size: 16px;
      width: 10px;
      color: #5d5d5d;
    }

    .MuiAlert-message {
      font-size: 11px;
      color: #5d5d5d;
    }
  }
`;
