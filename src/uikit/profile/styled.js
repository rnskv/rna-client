import styled from 'styled-components';
import { Typography } from '@material-ui/core';
import MUIChip from '@material-ui/core/Chip';

export const Box = styled.div`
  display: flex;
  padding: 20px;
`;

export const Information = styled.div`
  padding: 0 20px;
`;

export const Name = styled.div``;

export const Bio = styled.div`
  margin-top: 20px;
`;

export const Padding = styled.div`
  padding: 20px;
`;

export const Links = styled.div`
  display: flex;
  justify-content: center;
  padding: 0 20px 20px;
`;

export const Chip = styled(MUIChip)`
  margin: 5px;
  && {
    padding: 5px;
  }
`;
