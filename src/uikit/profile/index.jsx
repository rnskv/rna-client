import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Avatar, Divider, Typography, Button } from '@material-ui/core';
import { Information, Name, Bio, Box, Padding, Links, Chip } from './styled';
import SalaryList from 'uikit/salary-list';
import IncomeCounter from 'uikit/income-counter';
import useUser from 'hooks/useUser';
import Skeleton from '@material-ui/lab/Skeleton';
import LinkIcon from '@material-ui/icons/Link';
import VkIcon from '@iconify/icons-mdi/vk';
import { Icon } from '@iconify/react';

const Profile = ({ userId }) => {
  const user = useUser({ id: userId });
  return (
    <div style={{ maxWidth: '600px', margin: '0 auto' }}>
      <Box>
        {user.loading ? (
          <Skeleton
            style={{ minWidth: '100px' }}
            component="div"
            variant="circle"
            width={100}
            height={100}
          />
        ) : (
          <Avatar
            src={user.data.photo}
            style={{ width: '100px', height: '100px' }}
          >
            {user.data.name[0]}
            {user.data.lastname[0]}
          </Avatar>
        )}
        <Information>
          <Name>
            <Typography variant="h4">
              {user.loading ? (
                <Skeleton width={200} />
              ) : (
                `${user.data.name} ${user.data.lastname}`
              )}
            </Typography>
          </Name>
          <Bio>
            <Typography variant="h5">Краткая биография</Typography>
            <Typography
              variant="body2"
              style={{ marginTop: '10px', textIndent: '20px' }}
            >
              {user.loading ? (
                <div>
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={150} />
                </div>
              ) : (
                user.data.bio
              )}
            </Typography>
          </Bio>
        </Information>
      </Box>
      {!user.loading && (
        <Links>
          <Chip
            icon={<LinkIcon />}
            color="primary"
            component="a"
            clickable
            target="__blank"
            disabled={!user.data.wiki}
            href={user.data.wiki}
            label="Wikipedia"
          />
          <Chip
            icon={<Icon color="#fff" width="24" height="24" icon={VkIcon} />}
            color="primary"
            component="a"
            clickable
            target="__blank"
            disabled={!user.data.vkId}
            href={`https://vk.com/id${user.data.vkId}`}
            label="ВКонтакте"
          />
        </Links>
      )}
      <Divider />
      <Padding>
        <Typography variant="h4">Рассчет дохода</Typography>
      </Padding>
      <IncomeCounter
        title="Заработок за сегодня:"
        salary={user.data.salary}
        loading={user.loading}
      />

      <SalaryList salary={user.data.salary} loading={user.loading} />
    </div>
  );
};

export default Profile;
