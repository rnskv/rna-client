import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Button as MUIButton } from '@material-ui/core';

const Button = ({ children, loading, disabled, ...props }) => {
  return (
    <MUIButton disabled={disabled || loading} {...props}>
      {children}
      {loading && (
        <CircularProgress
          size={24}
          style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginTop: -12,
            marginLeft: -12,
          }}
        />
      )}
    </MUIButton>
  );
};

export default Button;
