import React, { useMemo } from 'react';
import { Box } from './styled';

function random(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

const Cloud = ({ value }) => {
  const x = useMemo(() => random(0, 100), []);
  const y = useMemo(() => random(0, 0), []);
  const delay = useMemo(() => random(0, 6000), []);
  const toX = useMemo(() => random(-50, 50), []);
  const toY = useMemo(() => random(-100, -300), []);

  return (
    <Box x={x} y={y} delay={delay} toX={toX} toY={toY}>
      ₽
    </Box>
  );
};

export default Cloud;
