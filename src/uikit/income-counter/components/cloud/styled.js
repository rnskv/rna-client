import styled, { keyframes, css } from 'styled-components';

const rotate = ({ toX, toY }) => keyframes`
  from {
    transform: translateY(0) translateX(0);
    opacity: 0.7;
  }

  to {
    transform: translateY(${toY}px) translateX(${toX}px);
    opacity: 0;
  }
`;

export const Box = styled.div`
  width: 30px;
  height: 30px;
  position: absolute;
  right: 0;
  bottom: 0;
  top: ${({ y }) => y}px;
  animation: ${({ toX, toY }) => css`
    ${rotate({
      toX,
      toY,
    })} 1s ease-out
    infinite
  `};
  animation-delay: ${({ delay }) => delay}ms;
  opacity: 0;
  border: 5px solid #f3ac00;
  z-index: 11;
  box-shadow: inset 0 0 5px 1px #00000059;
  background: #ffc539;
  border-radius: 50%;
  font-size: 20px;
  color: white;
  text-align: center;
  line-height: 30px;
  font-weight: bold;
`;
