import PropTypes from 'prop-types';
import React, { useState, useEffect, useMemo } from 'react';
import { Box, Background, Left, Right } from './styled';
import { Typography } from '@material-ui/core';
import { format } from 'helpers/money';
import { getMillisecondsToday } from 'helpers/time';
import { getSalaryVariants } from 'helpers/money';
import Skeleton from '@material-ui/lab/Skeleton';
import Cloud from './components/cloud';

const IncomeCounter = ({ salary, title, size, loading, withCoins }) => {
  const { millisecondly } = getSalaryVariants(salary);

  const [milliseconds, setMilliseconds] = useState(getMillisecondsToday());

  useEffect(() => {
    if (loading) return;

    const interval = setInterval(() => {
      setMilliseconds(getMillisecondsToday());
    }, 1000 / 10);

    return () => clearInterval(interval);
  }, [loading]);

  const income = useMemo(() => milliseconds * millisecondly, [
    millisecondly,
    milliseconds,
  ]);

  const formattedIncome = format(income, {
    fixed: income > 1000 ? 2 : 5,
  });

  const mocks = useMemo(() => new Array(10).fill(''), []);
  return (
    <Background>
      <Box size={size}>
        <Typography variant="body1" color="textPrimary">
          {title}
        </Typography>
        {size !== 'small' ? (
          <Typography
            variant={formattedIncome.length > 10 ? 'h5' : 'h4'}
            color="textPrimary"
            style={{ fontWeight: 'bold' }}
          >
            {loading ? (
              <Skeleton width={300} style={{ margin: '0 auto' }} />
            ) : (
              formattedIncome
            )}
          </Typography>
        ) : (
          <Typography
            variant={formattedIncome.length > 10 ? 'body2' : 'body1'}
            color="textPrimary"
            style={{ fontWeight: 'bold' }}
          >
            {loading ? (
              <Skeleton width={300} style={{ margin: '0 auto' }} />
            ) : (
              formattedIncome
            )}
          </Typography>
        )}

        <Typography variant="body2" color="textSecondary">
          начало отсчета 00:00
        </Typography>
      </Box>
      <Left>
        {withCoins &&
          mocks.map((i, index) => (
            <Cloud key={index} value={millisecondly * 1000} />
          ))}
      </Left>
      <Right>
        {withCoins &&
          mocks.map((i, index) => (
            <Cloud key={index} value={millisecondly * 1000} />
          ))}
      </Right>
    </Background>
  );
};

IncomeCounter.propTypes = {
  income: PropTypes.number,
  withCoins: PropTypes.bool,
};

IncomeCounter.defaultProps = {
  title: 'Сегодня вы заработали:',
  withCoins: true,
};

export default IncomeCounter;
