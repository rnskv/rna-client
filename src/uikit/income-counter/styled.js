import styled from 'styled-components';
import background from 'resources/images/jpg/income-background.jpg';

export const Box = styled.div`
  text-align: center;
  padding: ${({ size }) => (size === 'small' ? '20px 0 20px' : '20px 0 40px')};
  max-width: 100%;
  box-sizing: border-box;
  margin: 0 auto;
  position: relative;
  background: linear-gradient(0deg, #ffffff7a, white);
`;

export const Background = styled.div`
  background: url('${background}') 0 0;
  background-size: cover;
  position: relative;
`;

export const Left = styled.div`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 30px;
  height: 30px;
`;

export const Right = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
  width: 30px;
  height: 30px;
`;
