import styled from 'styled-components';

export const Background = styled.div`
  width: 100vw;
  min-height: 100vh;
  background: #edeef0;
  display: flex;
  align-items: center;
  flex-direction: column;
`;
