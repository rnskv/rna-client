import React from 'react';
import { Background } from './styled';
import Footer from 'uikit/footer';
import Header from 'uikit/header';

function Home({ children }) {
  return (
    <Background>
      <Header />
      {children}
      <Footer />
    </Background>
  );
}

export default Home;
