import styled from 'styled-components';

export const Background = styled.div`
  width: 100vw;
  min-height: 100vh;
  background: radial-gradient(#4c76a830, #ff000000);
  display: flex;
  justify-content: center;
  align-items: center;
`;
