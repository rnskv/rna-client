import React from 'react';
import { Background } from './styled';

function Welcome({ children }) {
  return <Background>{children}</Background>;
}

export default Welcome;
