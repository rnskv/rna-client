import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { MenuItem, Typography, Badge, Grid, Divider } from '@material-ui/core';
import { Avatar, ProductContainer } from './styled';
import { format } from 'helpers/money';
import { nFormatter } from 'helpers/numbers';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';

const PossiblePurchases = ({ salary }) => {
  const bank = salary;

  const data = useMemo(
    () => [
      {
        name: 'Кирпич',
        cost: 10,
        photo:
          'https://images.ru.prom.st/614746391_w640_h640_kirpich-pechnoj..',
      },
      {
        name: 'Lamborghini',
        cost: 13000000,
        photo: 'https://s.rdrom.ru/1/pubs/4/35456/gen340_1896101.jpg',
      },
      {
        name: 'Chevrolette camaro',
        cost: 2900000,
        photo:
          'https://motor.ru/imgs/2019/06/26/08/3427649/b30cb602272f67d141104553491358810d205790.jpg',
      },
      {
        name: 'Хлеб',
        cost: 50,
        photo: 'https://cooklikemary.ru/sites/default/files/xleb.jpg',
      },
      {
        name: 'Водка',
        cost: 300,
        photo:
          'https://avatars.mds.yandex.net/get-mpic/1514097/img_id2840001808389356636.jpeg/9hq',
      },
      {
        name: 'iPhone 11',
        cost: 56000,
        photo: 'https://img.mvideo.ru/Pdb/30045357b.jpg',
      },
      {
        name: 'LADA Vesta',
        cost: 556000,
        photo:
          'https://cdnimg.rg.ru/img/content/176/00/17/LV_AT_Sedan_d_850.jpg',
      },
      {
        name: 'MacBook Pro',
        cost: 200000,
        photo: 'https://www.ixbt.com/img/r30/00/02/25/30/IMG3355.jpg',
      },
      {
        name: 'Волнистый попугай',
        cost: 500,
        photo: 'https://i.ytimg.com/vi/CCubXhHRLM4/maxresdefault.jpg',
      },
      {},
    ],
    []
  );

  const products = useMemo(
    () =>
      data
        .filter((product) => {
          return product.cost <= bank;
        })
        .sort((next, prev) => {
          return next.cost < prev.cost ? 1 : -1;
        }),
    [bank, data]
  );

  return (
    <div>
      <Grid style={{ padding: '0 20px 20px' }} container alignItems="center">
        <Grid item xs>
          <Typography gutterBottom variant="h5">
            Бюджет:
          </Typography>
        </Grid>
        <Grid item>
          <Typography gutterBottom variant="h4">
            {format(salary, { fixed: 2 })}
          </Typography>
        </Grid>
      </Grid>

      {!products.length && (
        <Grid style={{ padding: '20px' }} container alignItems="center">
          <Typography variant="h5">Слишком низкий бюджет</Typography>
        </Grid>
      )}
      {products.map((product) => {
        const maxPhoto = 9;

        const avaliableCount = Math.floor(bank / product.cost);
        const count = avaliableCount > maxPhoto ? maxPhoto : avaliableCount;
        const photoArray = new Array(count).fill(product.photo);

        return (
          <ProductContainer key={product.name}>
            <MenuItem>
              <Grid flexDirection="column">
                <div
                  style={{
                    padding: '20px 0',
                    display: 'flex',
                    flexWrap: 'wrap',
                    alignItems: 'center',
                  }}
                >
                  {photoArray.map((photo, index) =>
                    index === photoArray.length - 1 &&
                    avaliableCount !== count ? (
                      <Badge
                        badgeContent={`+ ${nFormatter(
                          avaliableCount - maxPhoto
                        )} шт.`}
                        color="secondary"
                      >
                        <Avatar key={index} src={photo} />
                      </Badge>
                    ) : (
                      <Avatar
                        style={{
                          width: index ? '40px' : '90px',
                          height: index ? '40px' : '90px',
                        }}
                        key={index}
                        src={photo}
                      />
                    )
                  )}
                </div>
                <div>
                  <Typography variant="h5">{product.name}</Typography>

                  <Typography variant="body1">
                    Цена:{' '}
                    <span style={{ color: '#4c76a8' }}>
                      {format(product.cost)}
                    </span>
                  </Typography>
                  <Typography variant="body2">
                    Всего можно купить: <i>{avaliableCount} шт.</i>
                  </Typography>
                </div>
              </Grid>
            </MenuItem>
          </ProductContainer>
        );
      })}
    </div>
  );
};

PossiblePurchases.propTypes = {
  salary: PropTypes.number,
};

export default PossiblePurchases;
