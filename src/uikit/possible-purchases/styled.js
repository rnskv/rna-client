import styled from 'styled-components';
import { Avatar as MUIAvatar } from '@material-ui/core';

export const Avatar = styled(MUIAvatar)`
  && {
    margin-right: 10px;
    margin-bottom: 10px;
    box-shadow: 0 0 8px 0px #808080;
  }
`;

export const ProductContainer = styled.div`
  margin: 10px 0;
  &:first-child {
    margin: 0;
  }
  &:nth-child(even) {
    background: #f3f3f3;
  }
`;
