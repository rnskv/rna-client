import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Button, Typography } from '@material-ui/core';
import { Box, Wrapper, Icon } from './styled';
import SalaryList from 'uikit/salary-list';
import { Copyright } from '@material-ui/icons';
import vkIcon from '@iconify/icons-mdi/vk';
import instagramIcon from '@iconify/icons-mdi/instagram';
import facebookIcon from '@iconify/icons-mdi/facebook';

const Footer = () => {
  return (
    <Box>
      <Wrapper>
        <Button>
          <Icon color="#4b76a8" width="40" height="40" icon={vkIcon} />
        </Button>
        <Button>
          <Icon color="#4b76a8" width="40" height="40" icon={instagramIcon} />
        </Button>
        <Button>
          <Icon color="#4b76a8" width="40" height="40" icon={facebookIcon} />
        </Button>
      </Wrapper>
      <Wrapper>
        <Copyright />
        <Typography style={{ marginLeft: '5px' }} variant="body1">
          Money<b>Gram</b> {new Date().getFullYear()}
        </Typography>
      </Wrapper>
    </Box>
  );
};

export default Footer;
