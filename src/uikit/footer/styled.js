import styled from 'styled-components';
import { Typography } from '@material-ui/core';
import { Icon as Iconfy } from '@iconify/react';

export const Icon = styled(Iconfy)``;

export const Box = styled.div`
  display: flex;
  margin-top: 40px;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 10px;
  color: #6f6f6f;
`;
