module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      "jsx": true
    }
  },
  "settings": {
    "react": {
      "version": "latest"
    }
  },
  extends: ['react-app', 'plugin:react/recommended', 'plugin:prettier/recommended'],
  plugins: ['babel'], // add your custom rules here
  rules: {
    'react/prop-types': 0,
    "react-hooks/exhaustive-deps": 0
  },
};
