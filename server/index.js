const express = require('express');
const path = require('path');
const app = express();
const { createProxyMiddleware } = require('http-proxy-middleware');
const morgan = require('morgan');

app.use(express.static(path.join(__dirname, '../build')));

app.use(
  '/api',
  createProxyMiddleware({
    target: 'http://116.203.19.71:8080',
    changeOrigin: true,
    pathRewrite: {
      '^/api': '/',
    },
  }),
  () => {
    console.log('api request');
  }
);

app.use(morgan('combined'));

app.use(function (req, res) {
  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

app.listen(5000);
