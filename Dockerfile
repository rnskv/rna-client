FROM node:12-alpine AS build
WORKDIR /usr/src/app
COPY . .

RUN yarn install
RUN yarn build

FROM node:12-alpine AS release
WORKDIR /usr/src/app
COPY --from=build /usr/src/app/yarn.lock ./
COPY --from=build /usr/src/app/package.json ./
RUN yarn install
COPY --from=build /usr/src/app/build ./build
COPY --from=build /usr/src/app/server ./server
EXPOSE 5000
CMD [ "yarn", "start:prod" ]